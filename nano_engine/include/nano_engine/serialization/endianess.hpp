#pragma once

#include <array>
#include <cstdint>
#include <algorithm>

namespace nano_engine::serialization
{
	enum class PlatformEndianess
	{
		BigEndian,
		LittleEndian
	};

	constexpr PlatformEndianess DetectEndianess()
	{
		union {
			uint32_t i;
			char c[4];
		} testEndian = {0x01020304};

		// 04 03 02 01 -> Little

		// 01 02 03 04 -> Big

		return (testEndian.c[0] == 0x01) ? PlatformEndianess::BigEndian : PlatformEndianess::LittleEndian;
		
	}

	template<typename T>
	T SwapEndian(const T& val)
	{
		union {
			T val;
			std::array<std::uint8_t, sizeof(T)> raw;
		} src, dst;

		src.val = val;
		std::reverse_copy(src.raw.begin(), src.raw.end(), dst.raw.begin());
		return dst.val;
	}
}